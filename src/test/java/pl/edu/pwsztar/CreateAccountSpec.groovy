package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class CreateAccountSpec extends Specification {

    @Unroll
    def "should create account number #accountNumber for #user"() {
        given: "initial data"
        def bank = new Bank()
        when: "the account is created"
        def number = bank.createAccount()
        then: "check account number"
        number == accountNumber

        where:
        user   | accountNumber
        'John' | 0
        'Tom'  | 1
        'Mike' | 2
        'Todd' | 3
    }

    def "should create account with balance equals 0"() {
        given: "initial data"
        def bank = new Bank()
        when: "the account is created"
        def number = bank.createAccount()
        then: "account balance equals 0"
        bank.accountBalance(number) == 0
    }
}
