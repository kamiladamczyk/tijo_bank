package pl.edu.pwsztar

import spock.lang.Specification

class DepositSpec extends Specification {

    def "should deposit money if account exists"() {
        given: "initial data"
        def bank = new Bank()
        def accountNumber = bank.createAccount()
        when: "deposit money"
        def isDeposited = bank.deposit(accountNumber, 222)
        then: "money is deposited"
        isDeposited
    }

    def "should not deposit money if account does not exists"() {
        given: "initial data"
        def bank = new Bank()
        when: "deposit money"
        def isDeposited = bank.deposit(12, 22)
        then: "money is not deposited"
        !isDeposited
    }
}