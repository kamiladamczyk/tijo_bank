package pl.edu.pwsztar;

import pl.edu.pwsztar.entity.Account;
import pl.edu.pwsztar.repository.AccountRepository;
import pl.edu.pwsztar.repository.impl.AccountRepositoryImpl;

import java.util.Optional;

class Bank implements BankOperation {

    private final AccountRepository accountRepository;

    Bank() {
        this.accountRepository = new AccountRepositoryImpl();
    }

    public int createAccount() {
        return accountRepository.create().getNumber();
    }

    public int deleteAccount(int accountNumber) {
        Optional<Account> account = accountRepository.getByNumber(accountNumber);
        return account.map(Account::getBalance).orElse(ACCOUNT_NOT_EXISTS);
    }

    public boolean deposit(int accountNumber, int amount) {
        Optional<Account> account = accountRepository.getByNumber(accountNumber);
        if (account.isPresent()) {
            account.get().deposit(amount);
            return true;
        } else {
            return false;
        }
    }

    public boolean withdraw(int accountNumber, int amount) {
        Optional<Account> account = accountRepository.getByNumber(accountNumber);
        return account.map((a) -> a.withdraw(amount))
                .orElse(false);
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        Optional<Account> fromOptional = accountRepository.getByNumber(fromAccount);
        Optional<Account> toOptional = accountRepository.getByNumber(toAccount);
        if (fromOptional.isPresent() && toOptional.isPresent()) {
            return fromOptional.get().transfer(toOptional.get(), amount);
        } else {
            return false;
        }
    }

    public int accountBalance(int accountNumber) {
        Optional<Account> account = accountRepository.getByNumber(accountNumber);
        return account.map(Account::getBalance).orElse(ACCOUNT_NOT_EXISTS);
    }

    public int sumAccountsBalance() {
        return accountRepository.getAll()
                .stream()
                .mapToInt(Account::getBalance)
                .sum();
    }
}